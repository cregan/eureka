'use strict';
import React, {
  AppRegistry,
  Component,
  View,
  Text,
  TouchableOpacity
} from 'react-native';
import LevelStore from '../Stores/LevelStore';
import LevelActions from '../Actions/LevelActions';
import CTA from '../CTA';

class LevelCTA extends Component {

  getNextLevel() {
    return LevelStore.getLevel() + 1;
  }

  goToNextLevel() {
    this.props.hide();
    LevelActions.change(LevelStore.getLevel() + 1);
  }

  render() {
    return (
      <CTA onPress={() => this.goToNextLevel()}>
        Congratulations!
        Start Level {this.getNextLevel()} >
      </CTA>
    )
  }
}

AppRegistry.registerComponent('LevelCTA', () => LevelCTA);

module.exports = LevelCTA;
