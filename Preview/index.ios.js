'use strict';
import React, {
  AppRegistry,
  Component,
  View
} from 'react-native';
import styles from './styles';
import SolutionsStore from '../Stores/SolutionsStore';
import Solutions from '../Solutions'
import LevelLayout from '../LevelLayout';
import LevelStore from '../Stores/LevelStore';

class Preview extends Component {

  constructor(props) {
    super(props);
    let level = LevelStore.getLevel();
    this.state = {level}

    this.levelChangeHandler = this.levelChangeHandler.bind(this);
  }

  componentDidMount() {
    this.unsubLevels = LevelStore.listen(this.levelChangeHandler)
  }

  componentWillUnmount() {
    this.unsubLevels();
  }

  levelChangeHandler(level) {
    this.setState({level});
  }

  render() {
    return(
      <View style={styles.Preview}>
        <LevelLayout level={this.state.level}/>
      </View>
    )
  }
}

AppRegistry.registerComponent('Preview', () => Preview);

module.exports = Preview;
