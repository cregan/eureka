'use strict';

import React, {
    StyleSheet
} from 'react-native';

const styles = StyleSheet.create({
  Preview: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center'
  }

});

module.exports = styles;
