'use strict';

import React, {
    StyleSheet
} from 'react-native';

const styles = StyleSheet.create({
  GameBoard: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center'
  },

  rowsContainer: {
    borderColor: 'black',
    borderWidth: 1,
    padding: 2,
    borderRadius: 12

  }

});

module.exports = styles;
