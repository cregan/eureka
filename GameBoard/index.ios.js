'use strict';
import React, {
  AppRegistry,
  Component,
  View,
  Text,
  StyleSheet,
} from 'react-native';
import Row from '../Row';
import styles from './styles';
import SolutionsStore from '../Stores/SolutionsStore';
import Solutions from '../Solutions';
import LevelStore from '../Stores/LevelStore';
import GameBoardActions from '../Actions/GameBoardActions';
import LevelActions from '../Actions/LevelActions';

class GameBoard extends Component {

  constructor(props){
    super(props);
    this.makePieces();
    let level = LevelStore.getLevel();
    let rowLength = Solutions[level].rowLength
    let pieces = this.makePieces();

    this.state = {level, rowLength, pieces}
    this.levelChangeHandler = this.levelChangeHandler.bind(this);
  }

  componentDidMount() {
    this.unsubLevelChange = LevelStore.listen(this.levelChangeHandler);
  }

  componentWillUnmount() {
    this.unsubLevelChange();
  }

  levelChangeHandler(level) {
    let rowLength = Solutions[level].rowLength;
    let pieces = this.makePieces();
    this.setState({rowLength, pieces});
  }

  makePieces() {
    let tags = SolutionsStore.getTags();
    let pieces = [];
    tags.forEach((tag) => {
      tag.pieces.forEach((piece) => {
        piece.id = tag.id;
        pieces.push(piece);
      });
    });

    pieces = this.shuffle(pieces);
    return pieces;
  }

  shuffle(array) {
    var currentIndex = array.length, temporaryValue, randomIndex;

    while (0 !== currentIndex) {
      randomIndex = Math.floor(Math.random() * currentIndex);
      currentIndex -= 1;
      temporaryValue = array[currentIndex];
      array[currentIndex] = array[randomIndex];
      array[randomIndex] = temporaryValue;
    }

    return array;
  }

  renderRow(pieces, rowIndex) {
    return (
      <Row
        key={rowIndex + '_row'}
        index={rowIndex}
        rowLength={this.state.rowLength}
        pieces={pieces} />
    )
  }

  renderRows() {
    let rowLength = this.state.rowLength;
    let count = 0;
    let rows = [];
    let piecesForRow = [];
    this.state.pieces.forEach((piece, index) => {
      if (rowLength === count + 1 || index === this.state.pieces.length -1) {
        piecesForRow.push(piece);
        rows.push(this.renderRow(piecesForRow, rows.length));
        count = 0;
        piecesForRow = [];
      } else {
        piecesForRow.push(piece);
        count++;
      }
    })
    return rows;
  }

  render() {
    return (
      <View style={styles.GameBoard}>
        <View style={styles.rowsContainer}>
        {this.renderRows()}
        </View>
      </View>
    )
  }
}

AppRegistry.registerComponent('GameBoard', () => GameBoard);

module.exports = GameBoard;
