'use strict';
import React, {
  AppRegistry,
  Animated,
  Component,
  Text,
  View
} from 'react-native';
import Piece from '../../Piece';
import styles from './styles';

const layout = 'fly-your-kite';

class FlyYourKite extends Component {

  constructor(props) {
    super(props);
  }

  solvedCB() {
    // do something when a piece/level has been solved
  };

  render() {
    return(
      <Animated.View>
        <Piece
          solvedCB={this.solvedCB}
          layout='animated-triangle-left'
          id={this.props.getId('kite-left', layout)}/>
        <Piece
          solvedCB={this.solvedCB}
          layout='animated-triangle-right'
          id={this.props.getId('kite-right', layout)}/>
        <Piece
          layout='animated-triangle-right'/>
        <Piece
          layout='animated-triangle-right'/>
        <Piece
          layout='animated-triangle-right'/>
        <Piece
          layout='animated-triangle-right'/>
      </Animated.View>
    )
  }
}

AppRegistry.registerComponent('FlyYourKite', () => FlyYourKite);

module.exports = FlyYourKite;
