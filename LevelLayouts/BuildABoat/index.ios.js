'use strict';
import React, {
  AppRegistry,
  Animated,
  Component,
  Text,
  View
} from 'react-native';
import Piece from '../../Piece';
import styles from './styles';

const layout = 'build-a-boat';

class BuildABoat extends Component {

  constructor(props) {
    super(props);
  }

  render() {
    return(
      <Animated.View>
        <Text>FPO BuildABoat</Text>
      </Animated.View>
    )
  }
}

AppRegistry.registerComponent('BuildABoat', () => BuildABoat);

module.exports = BuildABoat;
