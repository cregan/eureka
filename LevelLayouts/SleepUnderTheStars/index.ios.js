'use strict';
import React, {
  AppRegistry,
  Animated,
  Component,
  Text,
  View
} from 'react-native';
import Piece from '../../Piece';
import styles from './styles';

const layout = 'sleep-under-the-stars';

class SleepUnderTheStars extends Component {

  constructor(props) {
    super(props);
  }

  render() {
    return(
      <Animated.View>
        <Text>FPO SleepUnderTheStars</Text>
      </Animated.View>
    )
  }
}

AppRegistry.registerComponent('SleepUnderTheStars', () => SleepUnderTheStars);

module.exports = SleepUnderTheStars;
