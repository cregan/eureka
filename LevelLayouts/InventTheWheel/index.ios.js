'use strict';
import React, {
  AppRegistry,
  Animated,
  Component,
  View
} from 'react-native';
import Piece from '../../Piece';
import styles from './styles';

const layout = 'invent-the-wheel';

class InventTheWheel extends Component {

  constructor(props) {
    super(props);
    this.state = {
      pan: new Animated.ValueXY()
    }

    this.solvedCB = this.solvedCB.bind(this);
  }

  solvedCB() {
    Animated.timing(this.state.pan.y, {toValue: 440}).start();
  };

  render() {
    return(
      <Animated.View style={{paddingTop: this.state.pan.y}}>
        <Piece solvedCB={this.solvedCB} styles={styles.wheel} layout='animated-circle' id={this.props.getId('wheel', layout)} />
      </Animated.View>
    )
  }
}

AppRegistry.registerComponent('InventTheWheel', () => InventTheWheel);

module.exports = InventTheWheel;
