'use strict';
import React, {
  AppRegistry,
  Animated,
  Component,
  Text,
  View
} from 'react-native';
import Piece from '../../Piece';
import styles from './styles';
import ForestTree from '../../ForestTree';

const layout = 'stand-tall-in-the-forest';

class StandTallInTheForest extends Component {

  constructor(props) {
    super(props);
  }

  render() {
    return(
      <Animated.View>
        <View style={{position: 'absolute', right: 100}}>
          <ForestTree static={true} size='medium' />
        </View>
        <View style={{position: 'absolute', right: 200}}>
          <ForestTree static={true} size='medium' />
        </View>
        <View style={{position: 'absolute', right: 75}}>
          <ForestTree size='medium' shade='light' id={this.props.getId('tree', layout)} />
        </View>
        <View style={{position: 'absolute', right: 0}}>
          <ForestTree static={true} size='medium' shade='light'/>
        </View>
        <View style={{position: 'absolute', right: -40}}>
          <ForestTree static={true} size='medium'/>
        </View>
         <View style={{position: 'absolute', right: -100}}>
          <ForestTree static={true} size='medium'/>
        </View>
      </Animated.View>
    )
  }
}

AppRegistry.registerComponent('StandTallInTheForest', () => StandTallInTheForest);

module.exports = StandTallInTheForest;
