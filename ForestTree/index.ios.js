'use strict';
import React, {
  AppRegistry,
  LayoutAnimation,
  Animated,
  Component,
  View
} from 'react-native';
import Piece from '../Piece';
import styles from './styles';

class ForestTree extends Component {

  constructor(props) {
    super(props);

    this.state = {
      offset: this.getOffSet()
    }
  }

  getOffSet() {
    if (!this.props.size) { return 15}
    switch(this.props.size) {
      case 'medium':
        return 25
        break;
    }
  }

  getShade() {
    if(this.props.shade) {
      if (this.props.shade === 'light') {
        return '#009b89';
      }
    }
    return '#007c6e'
  }

  render() {
    return(
      <View>
        <View style={styles.treeTrunk} />
        <Piece
          style={{position: 'absolute', top: this.state.offset * 0, borderBottomColor: this.getShade()}}
          layout='animated-triangle-up'
          {...this.props}
          />
        <Piece
          style={{position: 'absolute', top: this.state.offset * 1, borderBottomColor: this.getShade()}}
          layout='animated-triangle-up'
          {...this.props}
          />
        <Piece
          style={{position: 'absolute', top: this.state.offset * 2, borderBottomColor: this.getShade()}}
          layout='animated-triangle-up'
          {...this.props}
        />
        <Piece
          style={{position: 'absolute', top: this.state.offset * 3, borderBottomColor: this.getShade()}}
          layout='animated-triangle-up'
          {...this.props}
        />
        <Piece
          style={{position: 'absolute', top: this.state.offset * 4, borderBottomColor: this.getShade()}}
          layout='animated-triangle-up'
          {...this.props}
        />
        <Piece
          style={{position: 'absolute', top: this.state.offset * 5, borderBottomColor: this.getShade()}}
          layout='animated-triangle-up'
          {...this.props}
        />
      </View>
    )
  }
}

AppRegistry.registerComponent('ForestTree', () => ForestTree);

module.exports = ForestTree;
