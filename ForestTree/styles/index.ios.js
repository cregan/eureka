'use strict';

import React, {
    StyleSheet
} from 'react-native';

const styles = StyleSheet.create({
  treeTrunk: {
    position: 'absolute',
    width: 10,
    height: 100,
    backgroundColor: 'red'
  }
});

module.exports = styles;
