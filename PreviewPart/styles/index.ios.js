'use strict';

import React, {
    StyleSheet
} from 'react-native';

const styles = StyleSheet.create({
 PreviewPart: {
  flex: 1,
  margin: 1,
  backgroundColor: 'white',
  padding: 20
 },
 image: {
    width: 300,
    height: 300
  },
});

module.exports = styles;
