'use strict';
import React, {
  AppRegistry,
  Component,
  View,
  Image,
  Text
} from 'react-native';
import styles from './styles';
import Icon from 'react-native-vector-icons/FontAwesome';
import SolutionsStore from '../Stores/SolutionsStore';

class Preview extends Component {

  constructor(props) {
    super(props);
    this.state = {
      icon: this.props.icon,
      source: this.props.source,
      type: this.props.type,
      solved: false
    }

    this.solutionHandler = this.solutionHandler.bind(this);
  }

  componentDidMount() {
    this.unsubSolutions = SolutionsStore.listen(this.solutionHandler);
  }

  componentWillUnmount() {
    this.unsubSolutions();
  }

  solutionHandler(tag) {
    if (tag.source === this.state.source && tag.solved) {
      this.setState({solved: true});
    }
  }

  _solvedTypeOf() {
    switch(this.state.type) {
      case 'img':
        return
        <Image
          style={styles.image}
          source={require(this.props.source)}
          resizeMode="cover" />
      break;
      case 'para':
        return <Text>{this.props.source}</Text>
      break;
    }
  }

  _renderContent() {
    if (this.state.solved) {
      return this._solvedTypeOf();
    } else {
      return <Icon name={this.state.icon} size={30} color="#000" />
    }
  }

  render() {
    return(
      <Text style={styles.PreviewPart}>
       {this._renderContent()}
      </Text>
    )
  }
}

AppRegistry.registerComponent('Preview', () => Preview);

module.exports = Preview;
