'use strict';
import React, {
  AppRegistry,
  Component,
  View
} from 'react-native';
import styles from './styles';
import SolutionsStore from '../Stores/SolutionsStore';
import AnimatedCircle from '../AnimatedCircle';
import AnimatedTriangle from '../AnimatedTriangle';

class Piece extends Component {

  constructor(props) {
    super(props);
    this.state = {
      solved: false
    }
    this.solutionHandler = this.solutionHandler.bind(this);
  }

  componentDidMount() {
    this.unsubSolutions = SolutionsStore.listen(this.solutionHandler);
  }

  componentWillUnmount() {
    this.unsubSolutions();
  }

  renderPiece() {
    switch(this.props.layout) {
      case 'animated-circle':
        return <AnimatedCircle cb={this.props.solvedCB} solved={this.state.solved} {...this.props}/>
      break;
      case 'animated-triangle-left':
        return <AnimatedTriangle cb={this.props.solvedCB} solved={this.state.solved} {...this.props}/>
      break;
      case 'animated-triangle-right':
        return <AnimatedTriangle cb={this.props.solvedCB} solved={this.state.solved} {...this.props}/>
      break;
      case 'animated-triangle-up':
        return <AnimatedTriangle cb={this.props.solvedCB} solved={this.state.solved} {...this.props}/>
      break;
    }
  }

  solutionHandler(piece) {
    if (piece.id === this.props.id) {
      this.setState({
        solved: piece.solved
      });
    }
  }

  render() {
    let styles = this.props.styles;
    return(
      <View style={styles}  >
        {this.renderPiece()}
      </View>
    )
  }
}

AppRegistry.registerComponent('Piece', () => Piece);

module.exports = Piece;
