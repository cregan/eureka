'use strict';
import React, {
  AppRegistry,
  Component,
  View,
  Text,
  TouchableOpacity
} from 'react-native';
import CTA from '../CTA';
import LevelActions from '../Actions/LevelActions';

class CongratsCTA extends Component {

  playAgain() {
    LevelActions.gameComplete();
    this.props.startOver();
  }

  render() {
    return (
      <CTA onPress={() => this.playAgain()}>
        Congratulations!
        You finished (Phew!)  Play Again?
      </CTA>
    )
  }
}

AppRegistry.registerComponent('CongratsCTA', () => CongratsCTA);

module.exports = CongratsCTA;
