'use strict';
import React, {
  AppRegistry,
  Component,
  View,
  Text,
  TouchableOpacity
} from 'react-native';
import LevelStore from '../Stores/LevelStore';
import LevelActions from '../Actions/LevelActions';
import CTA from '../CTA';

class OutOfMovesCTA extends Component {

  tryAgain() {
    LevelActions.change(LevelStore.getLevel());
  }

  render() {
    return (
      <CTA onPress={() => this.tryAgain()}>
        Bummer! You're out of moves! Try Again?
      </CTA>
    )
  }
}

AppRegistry.registerComponent('OutOfMovesCTA', () => OutOfMovesCTA);

module.exports = OutOfMovesCTA;
