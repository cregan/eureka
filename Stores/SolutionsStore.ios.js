import { AsyncStorage } from 'react-native';
import Reflux from 'reflux';
import SolutionsActions from '../Actions/SolutionsActions';
import LevelPromptActions from '../Actions/LevelPromptActions';
import LevelActions from '../Actions/LevelActions';
import Solutions from '../Solutions';
import LevelStore from './LevelStore';
import TimerMixin from 'react-timer-mixin';
//import _ from 'underscore';

let SolutionsStore = Reflux.createStore({

  listenables: [SolutionsActions, LevelPromptActions, LevelActions],
  mixins: [TimerMixin],
  init(){
    this.submittedPieces = '';
    this.level = LevelStore.getLevel();
    this.rows = this.makeRows();
    this.solved = [];

    LevelStore.listen((level) => {
      this.level = level;
      this.resetLevelValues();
    });
  },

  resetLevelValues() {
    this.submittedPieces = '';
    this.solved = [];
    this.rows = this.makeRows();
    let tags = Solutions[this.level].tags;
    tags.forEach((tag) => {
      tag.solved = false;
      tag.isValid = false;
    })
  },

  makeRows() {
    let rowLength = Solutions[this.level].rowLength;
    let rows = [];
    for (let i = 0; i < rowLength; i++) {
      rows.push(["empty"]);
    }
    return rows;
  },

  getTags() {
    return Solutions[this.level].tags;
  },

  addToSolved(tag) {
    let exists = false;
    this.solved.forEach((solvedTag) => {
      if (solvedTag.id === tag.id) {
        exists = true;
      }
    });
    if (!exists) {
      this.solved.push(tag);
      this.checkLevel();
    }
  },

  checkForSolved(){
    let tags = Solutions[this.level].tags;
    let foundSolution = false;
    tags.forEach((tag) => {
      if (this.submittedPieces.indexOf(tag.solutionString) > -1) {
        this.addToSolved(tag);
        foundSolution = true;
        tag.solved = true;
        tag.isValid = true;
        this.submittedPieces = '';
        this.trigger(tag);
      }
    });
  },

  checkLevel() {
    if (this.solved.length === Solutions[this.level].tags.length) {
      this.setTimeout(() => LevelPromptActions.promptNextLevel(), 500);
    }
    this.trigger(this.solved[this.solved.length - 1]);
  },

  buildString(row) {
    let rowString = "";
    row.forEach((piece) => {
      rowString+= piece.val;
    });
    return rowString;
  },

  insertIntoRow(piece) {
    let row = this.rows[piece.startRow];
    if(!row) { return }
    let previousRow = row;
    if(row.length !== Solutions[this.level].rowLength) {
      row.splice([piece.startIndex], 0, piece);
    } else {
      row[piece.startIndex] = piece;
    }
    this.submittedPieces = this.buildString(row);;
    this.checkForSolved();
  },

  async onUnSolve(piece) {
    this.solved.forEach((tag, index) => {
      if (piece.id === tag.id) {
        tag.solved = false;
        tag.isValid = false;
        this.trigger(tag);
        this.solved.splice(index, 1);
      }
    });
  },

  async onReset() {
    this.submittedPieces = '';
  },

  async onSubmit(piece) {
    this.insertIntoRow(piece);
  },

  async onGameComplete() {
    this.resetLevelValues();
    this.level = 0;
  }

});

export default SolutionsStore;
