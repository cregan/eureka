import { AsyncStorage } from 'react-native';
import Reflux from 'reflux';
import LevelActions from '../Actions/LevelActions';
import Solutions from '../Solutions';

let LevelStore = Reflux.createStore({

  listenables: LevelActions,

  init() {
    this.level = 0;
    this.numLevels = Solutions.length;
  },

  getLevel() {
    return this.level;
  },

  async onGameComplete() {
    this.level = 0;
  },

  async onChange(level) {
    this.level = level;
    this.trigger(this.level);
  }


});

export default LevelStore;
