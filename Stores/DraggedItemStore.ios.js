import { AsyncStorage } from 'react-native';
import Reflux from 'reflux';
import SquareActions from '../Actions/SquareActions';

let DraggedItemStore = Reflux.createStore({

  listenables: SquareActions,

  init() {},

  async onMove(draggedValues) {
    this.trigger(draggedValues);
  },

  _getTargetRow(values) {
    switch (values.direction) {
      case 'up':
        return values.rowIndex - 1;
      case 'down':
        return values.rowIndex + 1;
      default:
        return values.rowIndex;
    }
  },

  _getTargetIndex(values) {
    return Math.floor(values.px / 60);
  }
});

export default DraggedItemStore;
