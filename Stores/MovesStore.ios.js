import { AsyncStorage } from 'react-native';
import Reflux from 'reflux';
import MoveActions from '../Actions/MoveActions';
import LevelStore from '../Stores/LevelStore';
import Solutions from '../Solutions';

let MovesStore = Reflux.createStore({

  listenables: MoveActions,

  init() {
    this.moves = Solutions[0].maxMoves;

    LevelStore.listen((level) => {
      this.moves = Solutions[level].maxMoves;
      this.trigger(this.moves);
    });
  },

  getMoves() {
    return this.moves;
  },

  async onTakeMove(level) {
    this.moves--;
    this.trigger(this.moves)
  }
});

export default MovesStore;
