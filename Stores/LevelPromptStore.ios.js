import { AsyncStorage } from 'react-native';
import Reflux from 'reflux';
import LevelStore from './LevelStore';
import LevelPromptActions from '../Actions/LevelPromptActions';

let LevelPromptStore = Reflux.createStore({

  listenables: [LevelPromptActions],

  init() {},

  async onPromptNextLevel() {
     this.trigger(true);
  },

});

export default LevelPromptStore;
