'use strict';
import React, {
  AppRegistry,
  Component,
  View,
  TouchableOpacity,
  Animated,
  PanResponder,
  StyleSheet,
  Text
} from 'react-native';
import styles from './styles';
import SolutionsActions from '../Actions/SolutionsActions';
import SquareActions from '../Actions/SquareActions';
import MoveActions from '../Actions/MoveActions';
import SolutionsStore from '../Stores/SolutionsStore';
import LevelStore from '../Stores/LevelStore';
import DraggedItemStore from '../Stores/DraggedItemStore';
import Dimensions from 'Dimensions';

let {width, height} = Dimensions.get('window');

class Square extends Component {
  constructor(props) {
    super(props);
    this.state = {
      pan: new Animated.ValueXY(),
      anchorPosX: null,
      val: this.props.val,
      anchorPosY: null,
      id: this.props.id,
      rowCount: 0,
      indexCount: 0,
      startIndex: this.props.index,
      startRow: this.props.rowIndex,
      dragging: false,
      order: this.props.order,
      isValid: false
    }

    this.state.pan.addListener((value) => {
      if(this.state.dragging) {
        this._onMove(value);
      }
    });

    this.draggedItemHandler = this.draggedItemHandler.bind(this);
    this.validHandler = this.validHandler.bind(this);
    this.levelChangeHandler = this.levelChangeHandler.bind(this);

    this._animatedValueX = 0;
    this._animatedValueY = 0;

    this.state.pan.x.addListener((value) => this._animatedValueX = value.value);
    this.state.pan.y.addListener((value) => this._animatedValueY = value.value);

    this.panResponder = PanResponder.create({
        onMoveShouldSetPanResponder : () =>  true,
        onMoveShouldSetPanResponderCapture: () => true,
        onPanResponderGrant: () => {
          this.state.pan.setOffset({x: this._animatedValueX, y: this._animatedValueY});
          this.state.pan.setValue({x: 0, y: 0});
          this.refs.square.measure((x, y, w, h, px, py) => {
            let anchorPosX = px + 30;
            let anchorPosY = py + 30;
            this.setState({
             dragging: true,
             anchorPosY: anchorPosY,
             anchorPosX: anchorPosX
         });
        });

        },
        onPanResponderMove           : Animated.event([null,{
          dx : this.state.pan.x,
          dy : this.state.pan.y
        }]),
        onPanResponderRelease        : (e, gesture) => {
          //TODO check for valid block;
          MoveActions.takeMove();
          SolutionsActions.submit(this.state);
          if (this.state.dragging) {
            this.setState({
              dragging: false
            });

            this.state.pan.flattenOffset();
            this.animateToRow();
            this.animateToCol();
          }
        }
    });
  }

  animateToCol(shouldSubmit) {
    Animated.spring(this.state.pan.x,
      { toValue: this.state.indexCount * 62}).start(() => {
       SolutionsActions.submit(this.state);
       this.setState({
         isTargetSibling: false,
       });

    });
  }

  animateToRow() {
    if (this.state.valid) {
      SolutionsActions.unSolve(this.state);
    }
    Animated.spring(this.state.pan.y,
      { toValue: this.state.rowCount * 62}).start(() => {
       SolutionsActions.submit(this.state);
       this.setState({
         isTargetSibling: false,
       });
    });
  }

  isTargetRowAbove(startRow, direction) {
    return (startRow === (this.state.startRow + 1)) && (direction === 'up');
  }

  isTargetRowBelow(startRow, direction) {
    return (startRow === (this.state.startRow - 1)) && (direction === 'down');
  }

  isAboveOrBelow(startRow, direction) {
    if (this.isTargetRowAbove(startRow, direction) ||
        this.isTargetRowBelow(startRow, direction)) {
        return true;
    }
    return false;
  }

  isNeighborToEmptySlot(draggedItemValues) {
    if (draggedItemValues.direction === 'left' || draggedItemValues.direction === 'right') {
      return;
    }
    if (draggedItemValues.startRow === this.state.startRow
      && this.state.startIndex >= draggedItemValues.startIndex) {
      return true;
    }
    return false;
  }

  isOrphaned(startRow, direction) {
    if (this.state.startIndex === this.props.length - 1 && this.isAboveOrBelow(startRow, direction)) {
      return true;
    }
    return false;
  }

  moveOrphan(draggedItemValues) {
    if (draggedItemValues.direction === 'up') {
      this.setState({
        rowCount: this.state.rowCount + 1,
        startRow: this.state.startRow + 1
      });
    } else {
      this.setState({
        rowCount: this.state.rowCount - 1,
        startRow: this.state.startRow - 1
      });
    }
    this.animateToRow();
  }

  isNeighborShouldMove(startIndex) {
    return this.state.startIndex >= startIndex;
  }

  isNeighborInTargetRow(draggedItemValues){
    if (this.state.startRow === draggedItemValues.startRow ||
        (this.isTargetRowAbove(draggedItemValues.startRow, draggedItemValues.direction) ||
        this.isTargetRowBelow(draggedItemValues.startRow, draggedItemValues.direction))
        && draggedItemValues.startIndex <= this.state.startIndex
        && !this.state.isTargetSibling && !this.state.dragging) {
        return true;
    }
    return false;
  }

  moveTargetNeighbor(draggedItemValues) {
    let direction = draggedItemValues.direction
    if (draggedItemValues.startRow === this.state.startRow) {
      if (direction === 'left') {
        if (this.state.startIndex === (draggedItemValues.startIndex - 1)) {
          this.setState({
            isTargetSibling: true,
            indexCount: this.state.indexCount + 1,
            startIndex: this.state.startIndex + 1
          });
          this.animateToCol();
        }
      } else if (direction === 'right') {
        if (this.state.startIndex === (draggedItemValues.startIndex + 1)) {
          this.setState({
            isTargetSibling: true,
            indexCount: this.state.indexCount - 1,
            startIndex: this.state.startIndex - 1
          });
          this.animateToCol();
        }
      }
    } else {
      this.setState({
        isTargetSibling: true,
        indexCount: this.state.indexCount + 1,
        startIndex: this.state.startIndex + 1
      });

      this.animateToCol();

    }
  }

  closeGap() {
    this.setState({
      indexCount: 0,
      isTargetSibling: false,
      startIndex: this.state.startIndex - 1
    });
    this.animateToCol();
  }

  closeNewGap() {
    this.setState({
      indexCount: this.state.indexCount - 1,
      startIndex: this.state.startIndex - 1
    });
    this.animateToCol();
  }

  componentDidMount() {
    this.unsubSolution = SolutionsStore.listen(this.validHandler)
    this.unsubDrag = DraggedItemStore.listen(this.draggedItemHandler);
    this.unsubLevel = LevelStore.listen(this.levelChangeHandler);
    SolutionsActions.submit(this.state);
  }

  validHandler(tag) {
    let thisstring = "" + tag.solutionString;
    if (thisstring.indexOf(this.props.val) > -1 && this.props.id === tag.id) {
      this.setState({valid: tag.isValid});
    }
  }

  draggedItemHandler(draggedItemValues) {
    if (this.state.dragging) { return }
    if (this.isOrphaned(draggedItemValues.startRow, draggedItemValues.direction)) {
      this.moveOrphan(draggedItemValues);
    } else if(this.isNeighborToEmptySlot(draggedItemValues)) {
      this.closeNewGap();
    } else if (this.isNeighborInTargetRow(draggedItemValues)) {
        this.moveTargetNeighbor(draggedItemValues);
    } else if (this.isAboveOrBelow(draggedItemValues.startRow, draggedItemValues.direction)) {
      if (this.isNeighborShouldMove()) {
        this.closeGap();
      }
    }
  }

  levelChangeHandler() {
    this.setState({valid: false});
  }

  componentWillUnmount() {
    this.state.pan.x.removeAllListeners();
    this.state.pan.y.removeAllListeners();
    this.unsubDrag();
    this.unsubSolution();
    this.unsubLevel();
  }

  getDirection(px, py) {
    if (py > this.state.anchorPosY) {
      return 'down';
    } else if ((this.state.anchorPosY - py) > 60) {
      return 'up';
    } else if ((this.state.anchorPosX - px) > 60) {
      return 'left';
    } else if (px > this.state.anchorPosX) {
      return 'right';
    }
  }

  _onMove(values) {
    this.refs.square.measure((x, y, w, h, px, py) => {
      values.direction = this.getDirection(px, py);
      values.rowCount = this.state.rowCount;
      values.indexCount = this.state.indexCount;
      values.startIndex = this.state.startIndex;
      values.startRow = this.state.startRow;
      let anchorPosY = this.state.anchorPosY;
      let anchorPosX = this.state.anchorPosX;
      let tempIndex = this.state.startIndex;
      let tempRow = this.state.startRow;
      let shouldMove = true;
      switch(values.direction) {
        case 'down':
          values.rowCount++;
          tempRow++;
          anchorPosY = py + 60;
        break;
        case 'up':
          values.rowCount--;
          tempRow--;
          anchorPosY = py;
        break;
        case 'left':
          values.indexCount--;
          tempIndex--;
          anchorPosX = px;
        break;
        case 'right':
          anchorPosX = px + 60;
          values.indexCount++;
          tempIndex++;
        break;
        default:
          shouldMove = false;
        break;
      }
      if (shouldMove) {
        values.targetIndex = this.state.startIndex + values.indexCount;
        SquareActions.move(values);

        this.setState({
          indexCount: values.indexCount,
          rowCount: values.rowCount,
          anchorPosX: anchorPosX,
          anchorPosY: anchorPosY,
          startIndex: tempIndex,
          startRow: tempRow
        })
      }

    });

  }

  getBackGroundColor() {
    if (this.state.valid) {
      return 'rgba(34,139,34,.5)';
    }
    return 'white';
  }

  getTransformStyles() {
    let backGrondCl = this.getBackGroundColor();
    return {
      backgroundColor: backGrondCl,
    };
  }

  getOrder() {
    switch(this.state.order) {
      case 'start':
        return styles.startSquare;
      break
      case 'middle':
        return
      break
      case 'end':
        return styles.endSquare
      break
    }
  }

  getDragState() {
    if (this.state.dragging) {
      return styles.squareDragging
    }
  }

  getFontColor() {
    let color = this.state.valid ? 'white' : 'black';
    return color;
  }

  getDisplayValue() {
    if (!this.state.dragging) {
      return 0;
    }
  }

  render() {
    return (
     <Animated.View
        {...this.panResponder.panHandlers}
        style={[{transform: this.state.pan.getTranslateTransform()},
                 styles.square, this.getOrder(), this.getDragState(),
                 this.getTransformStyles()]}>
        <View style={[styles.marker, {opacity: this.getDisplayValue()}]}><Text style={styles.markerText} >{this.props.val}</Text></View>
        <Text style={[styles.squareText, {color: this.getFontColor()}]} ref="square">
          {this.props.val}
        </Text>

      </Animated.View>
    );
  }
}

AppRegistry.registerComponent('Square', () => Square);

module.exports = Square;


