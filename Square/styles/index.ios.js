'use strict';

import React, {
    StyleSheet
} from 'react-native';

const styles = StyleSheet.create({
  square: {
    height: 60,
    width: 60,
    justifyContent: 'center',
    alignItems: 'center',
    borderColor: 'grey',
    borderRadius: 6,
    borderWidth: 1,
    margin: 1
  },

  marker: {
    position: 'absolute',
    top: -60,
    borderColor: 'grey',
    borderRadius: 4,
  },
  markerText: {
    fontSize: 10
  },
  squareDragging: {
    backgroundColor: 'black'
  },
   squareText: {
    fontSize: 9,
    textAlign: 'center',
    alignItems: 'center',
    fontWeight: 'bold'
  },
  startSquare: {
    borderColor: 'grey',
    borderBottomLeftRadius: 12,
    borderTopLeftRadius: 12,
  },
  endSquare: {
    borderColor: 'grey',
    borderBottomRightRadius: 12,
    borderTopRightRadius: 12,
  }
});

module.exports = styles;
