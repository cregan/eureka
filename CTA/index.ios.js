'use strict';
import React, {
  AppRegistry,
  Component,
  View,
  Text,
  TouchableOpacity
} from 'react-native';
import styles from './styles';

class CTA extends Component {
  render() {
    return (
      <TouchableOpacity style={styles.container} onPress={() => this.props.onPress()} >
        <View style={styles.innerContainer}>
          <Text style={styles.ctaText}>
           {this.props.children}
          </Text>
        </View>
      </TouchableOpacity>
    )
  }
}

AppRegistry.registerComponent('CTA', () => CTA);

module.exports = CTA;
