'use strict';
import React, {
    StyleSheet
} from 'react-native';

const styles = StyleSheet.create({
  container: {
    justifyContent: 'center',
    flexWrap: 'wrap',
    alignItems: 'center',
    backgroundColor: 'transparent',
    width: 376,
    flex: 1
  },

  innerContainer: {
    justifyContent: 'center',
    flexWrap: 'wrap',
    alignItems: 'center',
    height: 75,
    backgroundColor: 'rgba(0, 0, 0, 0.8)',
    width: 376
  },

  ctaText: {
    color: 'white'
  }

});

module.exports = styles;
