'use strict';
import React, {
  AppRegistry,
  LayoutAnimation,
  Animated,
  Component,
  View
} from 'react-native';
import styles from './styles';

class AnimatedTriangle extends Component {

  constructor(props) {
    super(props);
    let borderBottomWidth = 0;
    if (this.props.static) {
      borderBottomWidth = this.getBorderWidth();
    }

    this.state = {
      bw: new Animated.Value(borderBottomWidth),
      solved: false
    };
  }

  componentDidUpdate(nextProps) {
    if (nextProps.solved !== this.props.solved) {
      if (!this.props.static) {
        this.solve();
      }
    }
  }

  solve() {
    Animated.spring(this.state.bw, {toValue: this.getBorderWidth()}).start(() => {
      if (this.props.solved && this.props.cb) {
        this.props.cb();
      }
    });
  }

  getBorderWidth() {
    if (this.props.size) {
      switch (this.props.size) {
        case 'medium':
          return 40
          break;
      }
    }
    return 20
  }

  getSize() {
    if (this.props.size) {
      switch (this.props.size) {
        case 'medium':
          return styles.triangleMedium
          break;
      }
    }

    return styles.triangle;
  }

  render() {
    return(
      <View>
        <Animated.View style={
          [this.getSize(),
          {borderBottomWidth: this.state.bw},
          this.props.style]
        }>
        </Animated.View>
      </View>
    )
  }
}

AppRegistry.registerComponent('AnimatedTriangle', () => AnimatedTriangle);

module.exports = AnimatedTriangle;
