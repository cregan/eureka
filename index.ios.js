/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 */
'use strict';
import React, {
  AppRegistry,
  Component,
  StyleSheet,
  View,
  Modal,
  Text
} from 'react-native';
import LevelPromptStore from './Stores/LevelPromptStore';
import LevelStore from './Stores/LevelStore';
import MovesStore from './Stores/MovesStore';
import LevelActions from './Actions/LevelActions';
import LevelCTA from './LevelCTA';
import OutOfMovesCTA from './OutOfMovesCTA';
import CongratsCTA from './CongratsCTA';
import GameBoard from './GameBoard';
import StartScreen from './StartScreen';
import Solutions from './Solutions';
import Preview from './Preview';

class eureka_css extends Component {

  constructor(props) {
    super(props);
    this.state = {
      showLevelPrompt: false,
      showCongratsPrompt: false,
      task: Solutions[0].title,
      moves: MovesStore.getMoves(),
      phase: 'start'
    }

    this.levelPromptHandler = this.levelPromptHandler.bind(this);
    this.moveHandler = this.moveHandler.bind(this);
    this.levelChangeHandler = this.levelChangeHandler.bind(this);
  }

  componentDidMount() {
    this.unsubscribeMoves = MovesStore.listen(this.moveHandler);
    this.unsubscribePrompt = LevelPromptStore.listen(this.levelPromptHandler);
    this.unsubscribeLevels = LevelStore.listen(this.levelChangeHandler);
  }

  componentWillUnmount() {
    this.unsubscribeLevels();
    this.unsubscribePrompt();
    this.unsubscribeMoves();
  }

  levelPromptHandler() {
    if (LevelStore.getLevel() === Solutions.length - 1) {
      this.setState({showCongratsPrompt: true});
    } else {
      this.setState({showCongratsPrompt: false});
      setTimeout(() => {this.setState({showLevelPrompt: true})}, 2000)
    }
  }

  moveHandler(moves) {
    this.setState({moves});
  }

  levelChangeHandler(level) {
    this.setState({task: Solutions[LevelStore.getLevel()].title})
  }

  hideLevelPrompt() {
    this.setState({showLevelPrompt: false})
  }

  hideCongratsPrompt() {
    this.setState({showCongratsPrompt: false});
  }

  startOver() {
    this.setPhase('start');
    this.setState({showCongratsPrompt: false});
  }

  setPhase(phase) {
    this.setState({phase});
  }

  renderForPhase() {
    let screen;
    switch (this.state.phase) {
      case 'game':
        screen = this.renderGame();
      break;
      default:
        screen = this.renderStart();
      break;
    }
    return screen;
  }

  renderStart() {
    return <StartScreen onStart={() => this.setPhase('game')}/>
  }

  renderGame() {
    return (
      <View style={styles.container}>
        <Text>Moves left: {this.state.moves}</Text>
        <Preview />
        <GameBoard />
        <Text style={[{height: 40}, {paddingTop: 10}]}>{this.state.task}</Text>
        <Modal visible={this.state.showLevelPrompt} transparent={true} animated={true}>
          <LevelCTA hide={()=> this.hideLevelPrompt()}/>
        </Modal>
        <Modal visible={this.state.moves === 0} transparent={true} animated={true}>
          <OutOfMovesCTA />
        </Modal>
        <Modal visible={this.state.showCongratsPrompt} transparent={true} animated={true}>
          <CongratsCTA startOver={() => this.startOver()} />
        </Modal>
      </View>
    )
  }

  render() {
    return (
      <View style={styles.container}>
        {this.renderForPhase()}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'white',
  },
});

AppRegistry.registerComponent('eureka_css', () => eureka_css);
