'use strict';

import React, {
    StyleSheet
} from 'react-native';

const styles = StyleSheet.create({
  square: {
    width: 100,
    height: 100,
    borderWidth: 1,
    borderColor: 'black'
  },

  circle: {
    borderColor: 'black',
    borderWidth: 1,
    width: 100,
    height: 100,
    borderRadius: 50
  }

});

module.exports = styles;
