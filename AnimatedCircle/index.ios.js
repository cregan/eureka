'use strict';
import React, {
  AppRegistry,
  LayoutAnimation,
  Animated,
  Component,
  View
} from 'react-native';
import styles from './styles';

class AnimatedCircle extends Component {

  constructor(props) {
    super(props);
    this.state = {
      br: new Animated.Value(0),
      cw: new Animated.Value(50),
      solved: false
    };
  }

  componentDidUpdate(nextProps) {
    if (nextProps.solved !== this.props.solved) {
      this.solve();
    }
  }

  solve() {
    Animated.sequence([
      //Animated.spring(this.state.cw, {toValue: 45}),
      Animated.parallel([
        Animated.spring(this.state.br, {toValue: 50}),
        Animated.spring(this.state.cw, {toValue: 100})
      ])
    ]).start(() => {
      if (this.props.solved && this.props.cb) {
        this.props.cb();
      }
    });
  }

  render() {
    return(
      <View>
        <Animated.View style={
          [styles.square,
          {borderRadius: this.state.br,
           height: this.state.cw,
           width: this.state.cw
         }]
        }>
        </Animated.View>
      </View>
    )
  }
}

AppRegistry.registerComponent('AnimatedCircle', () => AnimatedCircle);

module.exports = AnimatedCircle;
