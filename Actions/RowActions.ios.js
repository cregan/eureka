import Reflux from 'reflux';

let RowActions = Reflux.createActions([
    "rowRegister",
]);

export default RowActions;
