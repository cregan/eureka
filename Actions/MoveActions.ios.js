import Reflux from 'reflux';

let MoveActions = Reflux.createActions([
    "takeMove",
]);

export default MoveActions;
