import Reflux from 'reflux';

let LevelPromptActions = Reflux.createActions([
    "promptNextLevel"
]);

export default LevelPromptActions;
