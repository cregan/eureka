import Reflux from 'reflux';

let SquareActions = Reflux.createActions([
    "move",
]);

export default SquareActions;
