import Reflux from 'reflux';

let SolutionsActions = Reflux.createActions([
    "submit",
    "unSolve",
]);

export default SolutionsActions;
