import Reflux from 'reflux';

let GameBoardActions = Reflux.createActions([
    "setBoard",
]);

export default GameBoardActions;
