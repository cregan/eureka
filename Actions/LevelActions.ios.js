import Reflux from 'reflux';

let LevelActions = Reflux.createActions([
    "change",
    "gameComplete",
]);

export default LevelActions;
