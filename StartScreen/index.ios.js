'use strict';
import React, {
  AppRegistry,
  Component,
  View,
  Text,
  TouchableOpacity
} from 'react-native';
import styles from './styles';

class StartScreen extends Component {
  render() {
    return (
      <View style={styles.screenContainer}>
        <Text>Paint By CSS</Text>
        <TouchableOpacity onPress={this.props.onStart}>
          <Text style={styles.startBtn}>
            Start Game
          </Text>
        </TouchableOpacity>
      </View>
    )
  }
}

AppRegistry.registerComponent('StartScreen', () => StartScreen);

module.exports = StartScreen;
