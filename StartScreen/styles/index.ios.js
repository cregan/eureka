'use strict';
import React, {
    StyleSheet,
    Dimensions
} from 'react-native';

let ScreenHeight = Dimensions.get("window").height;
const styles = StyleSheet.create({
  screenContainer: {
    height: ScreenHeight,
    justifyContent: 'center',
    alignItems: 'center'
  },
  startBtn: {
    borderRadius: 20,
    borderWidth: 1,
    padding: 10
  }
});

module.exports = styles;
