'use strict';
import React, {
    StyleSheet
} from 'react-native';

const styles = StyleSheet.create({
  Row: {
    flexDirection: 'row',
    justifyContent: 'center',
    flexWrap: 'wrap',
  },
  debug: {
    position: 'absolute',
    top: 100,
    right: 50
  }
});

module.exports = styles;
