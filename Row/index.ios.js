'use strict';
import React, {
  AppRegistry,
  Component,
  View,
  Text
} from 'react-native';
import Square from '../Square';
import styles from './styles';
import RowActions from '../Actions/RowActions';
import LevelStore from '../Stores/LevelStore';
import DraggedItemStore from '../Stores/DraggedItemStore';


class Row extends Component {

 constructor(props) {
   super(props)

   this.state = {
     pieces: this.props.pieces,
     rowString: "",
     newPieceText: ""
   }

 }

 renderSquaresForRow() {
    let rowOfPieces = [];
    this.props.pieces.forEach((piece) => {
      this.state.rowString+= piece.text;
      rowOfPieces.push(
        <Square
          order={piece.order}
          length={this.props.rowLength}
          id={piece.id}
          rowIndex={this.props.index}
          index={rowOfPieces.length}
          key={this.props.index + "_" + piece.text + rowOfPieces.length}
          val={piece.text} />)
    });
    return rowOfPieces;
  }

  render() {
    return (
      <View style={styles.Row} pointerEvents='box-none'>
        {this.renderSquaresForRow()}
        <Text style={styles.debug}>{this.state.newPieceText}</Text>
      </View>
    )
  }
}

AppRegistry.registerComponent('Row', () => Row);

module.exports = Row;
