'use strict';
import React, {
  AppRegistry,
  Animated,
  Component,
  View
} from 'react-native';
import Solutions from '../Solutions';
import InventTheWheel from '../LevelLayouts/InventTheWheel';
import FlyYourKite from '../LevelLayouts/FlyYourKite';
import StandTallInTheForest from '../LevelLayouts/StandTallInTheForest';
import SleepUnderTheStars from '../LevelLayouts/SleepUnderTheStars';
import BuildABoat from '../LevelLayouts/BuildABoat';

class LevelLayout extends Component {
  getId(name, layout) {
    let lv = Solutions.find((lv) => {return lv.layout === layout});
    let tg = lv.tags.find((tag) => {return tag.name === name});
    return tg.id;
  }

  renderLevelLayout() {
    switch(Solutions[this.props.level].layout) {
      case 'invent-the-wheel':
        return <InventTheWheel getId={this.getId}/>
      break;
      case 'fly-your-kite':
        return <FlyYourKite getId={this.getId}/>
      break;
      case 'stand-tall-in-the-forest':
        return <StandTallInTheForest getId={this.getId}/>
      break;
      case 'sleep-under-the-stars':
        return <SleepUnderTheStars getId={this.getId}/>
      break;
      case 'build-a-boat':
        return <BuildABoat getId={this.getId}/>
      break;
    }
  }

  render() {
    return(
      <View >
        {this.renderLevelLayout()}
      </View>
    )
  }
}

AppRegistry.registerComponent('LevelLayout', () => LevelLayout);

module.exports = LevelLayout;
