'use strict';

import React, {
} from 'react-native';

const Solutions = [
  {
    layout: 'invent-the-wheel',
    title: 'invent the wheel',
    rowLength: 3,
    tags: [
        {
          name: 'wheel',
          solutionString: 'border-radius:50%',
          id: 23586,
          pieces: [
            {text: 'border', order: 'start'},
            {text: '-radius:', order: 'middle'},
            {text: '50%', order: 'end'}],
          solved: false
      },
    ],
    maxMoves: 5
  },
  {
    layout: 'fly-your-kite',
    title: 'fly your kite',
    rowLength: 2,
    tags: [
      {
        name: 'kite-left',
        solutionString: 'border-width',
        id: 2003586,
        pieces: [
          {text: 'border', order: 'start'},
          {text: '-width', order: 'end'}],
        solved: false
      },
      {
        name: 'kite-right',
        solutionString: 'border-color',
        id: 28803586,
        pieces: [
          {text: 'border-', order: 'start'},
          {text: 'color', order: 'end'}],
        solved: false
      }

    ],
    maxMoves: 5
  },
  {
    layout: 'stand-tall-in-the-forest',
    title: 'stand tall in the forest',
    rowLength: 4,
    tags: [
        {
          name: 'tree',
          solutionString: 'border-bottom-width:40px',
          id: 2358688,
          pieces: [
            {text: 'border-', order: 'start'},
            {text: 'bottom-', order: 'middle'},
            {text: 'width', order: 'middle'},
            {text: ':40px', order: 'end'}],
          solved: false
      },
    ],
    maxMoves: 8
  },
  {
    layout: 'sleep-under-the-stars',
    title: 'sleep under the stars',
    rowLength: 3,
    tags: [
        {
          name: 'stars-color',
          solutionString: 'background-color:#FCEE9D',
          id: 2358654,
          pieces: [
            {text: 'background', order: 'start'},
            {text: '-color', order: 'middle'},
            {text: ':#FCEE9D', order: 'end'}],
          solved: false
      },
      {
          name: 'stars-shape',
          solutionString: 'background-color:#003366',
          id: 23444555,
          pieces: [
            {text: 'background', order: 'start'},
            {text: '-color:', order: 'middle'},
            {text: '#003366', order: 'end'}],
          solved: false
      }
    ],
    maxMoves: 8
  },
  {
    layout: 'build-a-boat',
    title: 'Build a boat',
    rowLength: 4,
    tags: [
      {
        name: 'sun-background',
        solutionString: 'background-color:#4A5267',
        id: 293586,
        pieces: [
          {text: 'background', order: 'start'},
          {text: '-color:', order: 'middle'},
          {text: '#', order: 'middle'},
          {text: '4A5267', order: 'end'}],
        solved: false
      },
      {
        name: 'boat-bottom-left',
        solutionString: 'border-bottom-left-radius:75%',
        id: 999876,
        pieces: [
          {text: 'border-', order: 'start'},
          {text: 'bottom-left-', order: 'middle'},
          {text: 'radius', order: 'middle'},
          {text: ':75%', order: 'end'}],
        solved: false
      },
      {
        name: 'sun-shape',
        solutionString: 'border-radius:50%',
        id: 112276,
        pieces: [
          {text: 'border', order: 'start'},
          {text: '-radius:', order: 'middle'},
          {text: '50', order: 'middle'},
          {text: '%', order: 'end'}],
        solved: false
      },
      {
        name: 'sail',
        solutionString: 'height: 200px',
        id: 1229876,
        pieces: [
          {text: 'height: ', order: 'start'},
          {text: '200', order: 'middle'},
          {text: 'p', order: 'middle'},
          {text: 'x', order: 'end'}],
        solved: false
      }
    ],
    maxMoves: 25
  }
]

module.exports = Solutions;
